// 2
package Robot;
public class Senzor{
    int value;
    boolean active;
    void setActive(){
        active = !active;
        System.out.println("Sensor active status="+active);
    }

    void setValue(int k){
        if(k<0 || k>100)
            System.out.println("Invalid value. Senzor value unchanged!");
        else{
            value = k;
            System.out.println("Sensor value="+value);
        }
    }

    int getValue(){
        return value;
    }


    public static void main(String[] args) {
        Senzor s1 = new Senzor();
        Senzor s2 = new Senzor();
        Senzor s3 = null;

        s1.setActive();
        s1.setValue(190);
        s1.setValue(67);

        System.out.println("Senzor s1 value="+s1.getValue());
        System.out.println("Senzor s2 value="+s2.getValue());
        s2 = s1;
        s3 = s1;
        System.out.println("...");
        System.out.println("Senzor s2 value="+s2.getValue());
        System.out.println("Senzor s3 value="+s3.getValue());
    }
}
